#include <iostream>
#include "Student.h"
#include "Pass.h"
//#include<string>

//Q2a: Define Display for Pass class
// Define the method display that you declared within the Pass class in the header file
// Information should be printed in the following format:
// Name: <name>
// Standard: <standard>
// Result: pass 
// (See the print_all function in hw10.cpp for proper use of this function.)

void Pass::display()
{
	Student::display();
	
	cout << "Name: " << getName() << endl;
	cout << "Standard: " << getStandard() << endl;
	cout << "Result: " << "Pass" << endl;
}