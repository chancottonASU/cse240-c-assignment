#include "Student.h"

Student::Student(string student_name, string student_standard, Result student_result) {
	name = student_name;
	standard = student_standard;
	result = student_result;
}

		string Student::getName() {
			return name;
		}

		string Student::getStandard() {
			return standard;
		}

		Result Student::getResult() {
			return result;
		}