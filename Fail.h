#pragma once

// Q1b: Create Fail Class
// Part 1: Create a child class of the Student class named 'Fail'
// See the add function in hw10.cpp for proper use of this function.
class Fail : public Student {
private: 
	
public:
	Fail(string student_name, string student_standard, Result student_result) :
		Student(student_name, student_standard, student_result) {};
	
	
	virtual void display();
};
// Part2: Declare constructor which accepts the same 3 parameters as the parent class Student.
// Pass the 3 parameters to the super constructor in the Student class.
// Part 3: Re-declare the method display (virtual method found inside of parent class Student)